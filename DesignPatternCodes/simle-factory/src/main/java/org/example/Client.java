package org.example;

import org.example.abstractmodel.ImageReader;
import org.example.abstractmodel.ImageReaderFactory;

/**
 * @author nanfang
 * 客户端调用类
 */
public class Client {

    public static void main(String[] args) {
        String type = "gif";
//        ImageReader imageReader = ImageReaderFactory.getImageReader(type);
//        imageReader.readImage();
        ImageReader readerByType = ImageReaderFactory.getReaderByType(type);
        readerByType.printlabel();
        readerByType.readImage();
    }
}
