package org.example.abstractmodel;

/**
 * @author nanfang
 * 工厂类
 */
public class ImageReaderFactory {
    public static ImageReader getReaderByType(String type){
        ImageReader imageReader = null;
        switch (type){
            case "jpg":
                imageReader = new JpgReader();
                break;
            case "gif":
                imageReader = new GifReader();
            default:

        }
        return imageReader;
    }
}
