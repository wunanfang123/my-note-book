package org.example.abstractmodel;

/**
 * @author nanfang
 * 简单工厂模式下的抽象类方式
 */
public abstract class ImageReader {
    /**
     * 读取不同类型文件的抽象方法
     */
    public abstract void readImage();

    public void printlabel(){
        System.out.println("我均来自ImageReader这个抽象类");
    }
}
