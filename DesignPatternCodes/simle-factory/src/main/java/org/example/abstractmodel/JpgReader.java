package org.example.abstractmodel;

/**
 * @author nanfang
 * 具体实现类-Jpg类
 */
public class JpgReader extends ImageReader{
    @Override
    public void readImage() {
        System.out.println("我是来自抽象类的具体实现类JpgReader");
    }
}
