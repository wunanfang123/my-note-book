package org.example.interfacemodel;

/**
 * Hello world!
 *
 * @author nanfang
 * 简单抽象工厂设计模式
 * 抽象设计接口
 */
public interface ImageReader {
    /**
     * 读取指定格式的图像文件
     */
    void readImage();
}

