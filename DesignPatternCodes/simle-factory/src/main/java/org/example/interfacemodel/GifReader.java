package org.example.interfacemodel;

/**
 * @author nanfang
 * 具体实现接口类-Gif
 */
public class GifReader implements ImageReader{
    @Override
    public void readImage() {
        System.out.println("读取的是Gif文件");
    }
}
