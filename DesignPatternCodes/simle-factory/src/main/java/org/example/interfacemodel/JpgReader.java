package org.example.interfacemodel;

/**
 * @author nanfang
 * 具体实现接口类-Jpg
 */
public class JpgReader implements ImageReader{
    @Override
    public void readImage() {
        System.out.println("读取的是jpg文件");
    }
}
