package org.example.interfacemodel;

/**
 * @author nanfang
 * 具体对象的实现工厂
 */
public class ImageReaderFactory {

    public static ImageReader getImageReader(String type){
        ImageReader imageReader = null;
        if ("jpg".equals(type)){
            imageReader = new JpgReader();
        }
        if ("gif".equals(type)){
            imageReader = new GifReader();
        }
        return imageReader;
    }
}
