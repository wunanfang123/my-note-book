package org.example;

/**
 * @author nanfang
 * 产品的抽象工厂
 */
public interface ImageReaderFactory {
    /**
     * 获取Reader的抽象方法
     * @return 返回Reader对象
     */
    ImageReader getImageReader();
}
