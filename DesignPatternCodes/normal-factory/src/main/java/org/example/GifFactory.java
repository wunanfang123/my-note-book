package org.example;

/**
 * @author nanfang
 */
public class GifFactory implements ImageReaderFactory{
    @Override
    public ImageReader getImageReader() {
        return new GifReader();
    }
}
