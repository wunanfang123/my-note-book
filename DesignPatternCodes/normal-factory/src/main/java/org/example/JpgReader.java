package org.example;

/**
 * @author nanfang
 * 子类的具体实现
 */
public class JpgReader implements ImageReader{
    @Override
    public void showReaderInfo() {
        System.out.println("我是JpgReader");
    }
}
