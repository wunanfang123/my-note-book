package org.example;

/**
 * @author nanfang
 * 客户端
 */
public class Client {
    public static void main(String[] args) {
        try {
            Class<?> aClass = Class.forName("org.example.JpgFactory");
            ImageReaderFactory imageReaderFactory = (ImageReaderFactory) aClass.newInstance();
            ImageReader imageReader = imageReaderFactory.getImageReader();
            imageReader.showReaderInfo();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
