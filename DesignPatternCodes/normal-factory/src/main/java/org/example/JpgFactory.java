package org.example;

/**
 * @author nanfang
 * Jpg工厂
 */
public class JpgFactory implements ImageReaderFactory{
    @Override
    public ImageReader getImageReader() {
        return new JpgReader();
    }
}
