package org.example;

/**
 * Hello world!
 *
 * @author nanfang
 * 工厂模式的抽象类，实例化延迟到子类
 * 先定义行为，再定义对象获取
 */
public interface ImageReader {
    /**
     * 展示各个读取器的信息
     */
    void showReaderInfo();
}
