package org.example;

/**
 * @author nanfang
 */
public class LinuxPlatformFactory implements PlatformFactory{
    @Override
    public Button createButton() {
        return new LinuxButton();
    }

    @Override
    public Text createText() {
        return new LinuxText();
    }
}
