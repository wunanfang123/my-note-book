package org.example;

/**
 * @author nanfang
 */
public class WindowsPlatformFactory implements PlatformFactory{
    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public Text createText() {
        return new WindowsText();
    }
}
