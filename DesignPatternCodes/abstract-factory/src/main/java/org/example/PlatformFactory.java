package org.example;

/**
 * @author nanfang
 */
public interface PlatformFactory {
    /**
     * 创建平台Button抽象方法
     * @return Button
     */
    Button createButton();

    /**
     * 创建平台Text抽象方法
     * @return Text
     */
    Text createText();
}
