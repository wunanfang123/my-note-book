package org.example;

/**
 * Hello world!
 *
 * @author nanfang
 * 抽象的Button类
 */
public interface Button {
    /**
     * 抽象的button方法
     */
    void showButton();
}
