package org.example;

/**
 * @author nanfang
 * 抽象的Text类
 */
public interface Text {
    /**
     * 抽象的Text方法
     */
    void showText();
}
