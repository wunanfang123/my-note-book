package org.example;

/**
 * @author nanfang
 */
public class Client {
    public static void main(String[] args) {
        LinuxPlatformFactory linuxPlatformFactory = new LinuxPlatformFactory();
        linuxPlatformFactory.createButton().showButton();
        linuxPlatformFactory.createText().showText();
        WindowsPlatformFactory windowsPlatformFactory = new WindowsPlatformFactory();
        windowsPlatformFactory.createButton().showButton();
        windowsPlatformFactory.createText().showText();
    }
}
