## UML类图

### 1. UML表示具体类

具体类在类图中用矩形框表示，矩形框分为三层：第一层是**类的名字**，第二层是类的**成员变量**，第三层是**类的方法**，成员变量以及方法前的访问修饰符用符号来表示：

- “+”表示`public`
- "-"表示`private`
- "#"表示`protected`
- 不带符号表示`default`

![image-20211120183103570](https://gitee.com/wunanfang123/pic-md1/raw/master/image-20211120183103570.png)

### 2. UML表示抽象类

抽象类同样用矩形框表示，但是抽象类的类名以及抽象方法的名字都是用斜体字表示。

![image-20211120185546461](https://gitee.com/wunanfang123/pic-md1/raw/master/image-20211120185546461.png)

### 3. UML表示接口

接口在类图中也是使用矩形框表示的，一般有两种表示方法：第一种是在顶部用单词interface单独标记，第二种则叫做棒棒糖表示法，就是一个圆圈连接一根实线

### 4.1 实现关系（接口及其实现类之间的关系）

实现关系用**空心三角和虚线**组成的箭头来表示，从**实现类指向接口**，其含义可以理解为`implements`

![image-20211120191146187](https://gitee.com/wunanfang123/pic-md1/raw/master/image-20211120191146187.png)

### 4.2 泛化关系

泛化关系指的是对象和对象之间的继承关系，泛化关系使用**空心三角和实线**组成的箭头表示，从子类指向父类，对象之间的泛化关系可以直接翻译为关键字`extends`

![image-20211120222517586](https://gitee.com/wunanfang123/pic-md1/raw/master/image-20211120222517586.png)

### 4.3 依赖关系

依赖关系用一个**带虚线的箭头**表示，由使用方指向被使用方，表示使用方持有被使用方的对象的引用

![image-20211120223420312](https://gitee.com/wunanfang123/pic-md1/raw/master/image-20211120223420312.png)

依赖关系的具体表现有以下几种情况：

- B为A的构造器中的局部变量
- B为A的方法中的局部变量
- B为A的方法参数
- B为A的构造器的参数
- B为A的方法的返回值
- A调用B的静态方法

举例如下

```java
//代码清单1 B.java
public class B {
  public String field1;   //成员变量

  public void method1() {
    System.println("在类B的方法1中");
  }

  public static void method2() {                 //静态方法
    System.out.println("在类B的静态方法2中");
  }
}

/* 代码清单2 A.java
  A依赖于B
*/

public class A {
  public void method1() {
    //A依赖于B的第一种表现形式：B为A的局部变量
    B b = new B();
    b.method1();
  }

  public void method2() {
    //A依赖于B的第二种表现形式： 调用B的静态方法
    B.method2();
  }

  public void method3(B b)  {
    //A依赖于B的第三种表现形式：B作为A的方法参数
    String s = b.field1;
  }

  //A依赖于B的第四种表现形式：B作为A的方法的返回值
  public B method4() {
    return new B();
  }
}
```

