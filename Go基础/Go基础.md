## Go的数组

数组存放的是**固定长度，相同类型**的数据，而且这些存放的元素是**连续**的

数组的声明方式如下：

```go
// 直接声明固定长度大小
array := [5]string{"a", "b", "c", "d", "e"}

// 忽略固定长度的大小声明，只适用于所有元素都被初始化的
```

数组在内存中是**连续存放**的

![](C:\Users\zhuifeng\AppData\Roaming\marktext\images\2022-01-04-15-14-39-image.png)

```go
// 使用for range循环获得数组索引以及数组的
for i, v := range arr {
        fmt.Printf("数组索引:%d, 对应值:%s \n", i, v)
    }
```

## Go的切片--动态数组

产生方式

- 从数组中切割生成，注意这种方式生成的切片，更改切片中的值，会影响数组中对应的值

- make函数声明切片

make函数声明的方式

```go
// 这表明Go在内存中划分了一个容量为8的内存空间，但是只有4个内存空间才有元素
// 其他的内存空间处于空闲状态，当使用append追加元素时，会追加到空闲内存上，超过容量会扩容
slice1 := make([]string, 4, 5)
```

## Go的Map

Map是一个无序的K-V键值对

map的声明初始化

```go
nameAgeMap := make(map[string]int)
```

map如果获取了不存在的K-V键值对，比如Key不存在，那返回的Value就是该类型的零值。因此获取之前要对map的key进行针对性判空

```go
nameAgeMap := make(map[string]int)
nameAgeMap["age"] = 20
nameAgeMap["address"] = 30
fmt.Println(nameAgeMap["name"])
val, status := nameAgeMap["age1"]
if status {
    fmt.Println(val)
}
```

遍历map

```go
for k, v := range nameAgeMap {
    fmt.Println(k, v)
}
```

## Go的函数

在 Go 语言中，**函数也是一种类型**，它也可以被用来声明函数类型的变量、参数或者作为另一个函数的返回值类型。

```go
func sum(a, b int) (int, error) {
    if a < 0 || b < 0 {
        return 0, errors.New("a或b不能为负数")
    }
    return a + b, nil
}


result, err := sum(-1, 2)
if err != nil {
    fmt.Println(err)
} else {
    fmt.Println(result)
}
```

定义可变参数，只需要在参数类型前加三个点即可，可变参数的实质是切片

```go
// 定义了一个可变
func sum1(params ...int) int {
    sum := 0
    for _, i := range params {
        sum += i
    }
    return sum
}
```

注意！！函数中若有普通参数和可变参数，那么可变参数一定要放在参数列表的最后一个！

#### 函数的作用域区分

- 函数名称首字母小写代表私有函数，只有在同一个包中才可以被调用

- 函数名称首字母大写代表公有函数，不同的包也可以调用

- 任何一个函数都会从属于一个包

> Go中没有private public等修饰符，通过函数名称大小来代表

#### 匿名函数和闭包

匿名函数示例

```go
func main() {

    sum2 := func(a, b int) int {
        return a + b
    }

    fmt.Println(sum2(2, 4))
}
```

注意，上述的sum2是一个函数类型的变量，而不是返回值

## Go的方法

在Go中，函数和方法是两个概念，区别在于，方法必须拥有一个接收者，这个接收者是一个类型，这样方法和这个类型就绑定在一起，称为这个类型的方法

```go
type Age uint

// 此时Age就是方法的接收者，方法String()就是类型Age的方法
func (age Age) String(){
    fmt.Println(age)
}
```

> 和函数不同，定义方法时会在关键字 func 和方法名 String 之间加一个接收者 (age Age) ，接收者使用小括号包围。

## Go的struct

结构体是一种聚合类型，里面可以包含任意类型的值成员。在Go中，要定义一个结构体，需要使用`type + struct`关键字组合

```go
type person struct {
    name string
    age uint
}
// 结构体定义模板
type structName struct{
    fieldName typeName
    ....
    ....
}

// 使用
var p person

p := person{"a", 20}
```

结构体支持嵌套，例如如下的实例

```go
type person struct {
	name string
	age  uint
	addr address
}

type address struct {
	province string
	city     string
}

func main() {
	p := person{
		age:  30,
		name: "kitty",
		addr: address{
			city:     "beijing",
			province: "hebei",
		},
	}

	fmt.Println(p.addr.city)
}
```

## Go的接口

接口是和调用方的一种约定，它是一种高度抽象的类型，不用和具体的实现细节绑定在一起。接口要做好的就是定义好约定，告诉调用方自己可以做什么，但不用知道它的内部实现。

interface{} 是空接口的意思，在 Go 语言中代表任意类型。

接口的定义关键字叫做`interface`，表示自定义的类型是一个接口

```go
type Stringer interface {
    String() string
}
```

实现接口必须要指定具体类型，即类型的方法

```go
type WalkRun interface {
	Walk()
	Run()
}

func (p person) Walk() {
	fmt.Printf("%s能走\n", p.name)
}

func (p person) Run() {
	fmt.Printf("%s能跑\n", p.name)
}
```

## Go的错误处理

简单自定义抛出的异常信息

```go
// 使用errors.New定义抛出的异常
func add(a, b int) (int, error) {
	if a < 0 || b < 0 {
		return 0, errors.New("a或b不能为负数")
	} else {
		return a + b, nil
	}
}
```

如何捕获异常并进行判断？

```go
i, err := strconv.Atoi("a")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(i)
	}
```

自定义error的错误信息

```go
type commonError struct {
	errorCode int
	errorMsg  string
}

// 方法的接受者
func (ce *commonError) Error() string {
	return ce.errorMsg
}
```

## Go的Defer

defer关键字用于修饰一个函数或者方法，使得该函数或者方法在返回前才会执行，也就是说被延迟执行，但又可以保证一定会执行

```go
func ReadFile(filename string) ([]byte, error) {
   f, err := os.Open(filename)
   if err != nil {
      return nil, err
   }
   defer f.Close()
   //省略无关代码
   return readAll(f, n)
}
```

上述被defer修饰的f.Close方法延迟执行，这意味着会先执行readAll方法，然后在整个ReadFile函数return前执行f.Close方法


